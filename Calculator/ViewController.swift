//
//  ViewController.swift
//  Calculator
//
//  Created by Chhaileng Peng on 6/2/18.
//  Copyright © 2018 Chhaileng Peng. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var lblOperation: UILabel!
    var i = 0
    var operand1 : Double = 0
    var operand2 : Double = 0
    var result : Double = 0
    var operation = 0
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func digitPressed(_sender: UIButton){
        if _sender.tag == 10 {
            if lblResult.text?.contains(".") == false {
                lblResult.text! += "."
            }
        }
        else{
            if lblResult.text! == "0" {
                lblResult.text = ""
            }
            lblResult.text! += String(_sender.tag)
        }
    }
    
    @IBAction func plusMinus(_ sender: Any) {
        if Double(lblResult.text!)! > 0 {
            lblResult.text = "-" + lblResult.text!
        }
        else{
            lblResult.text = checkNumber(number: String(Double(lblResult.text!)! * -1))
        }
    }
    @IBAction func operatorPressed(_sender: UIButton){
        //var temResult : Double = 0
        i += 1
        if lblResult.text != "" && _sender.tag != 7 && _sender.tag != 0 {
            if _sender.tag == 1 {
                if i > 1 {
                    operand2 = Double(lblResult.text!)!
                    result = calcalationSubTotal(operand1: operand1, operand2: operand2, previousOperation: operation)
                    operand1 = result
                }
                else {
                    operand1 = Double(lblResult.text!)!
                    result = operand1
                }
                lblOperation.text = checkNumber(number: formatPrecision(formated: result)) + " + "
            }
            if _sender.tag == 2 {
                if i > 1 {
                    operand2 = Double(lblResult.text!)!
                    result = calcalationSubTotal(operand1: operand1, operand2: operand2, previousOperation: operation)
                    operand1 = result
                }
                else {
                    operand1 = Double(lblResult.text!)!
                    result = operand1
                }
                lblOperation.text = checkNumber(number: formatPrecision(formated: result)) + " - "
            }
            if _sender.tag == 3 {
                if i > 1 {
                    operand2 = Double(lblResult.text!)!
                    result = calcalationSubTotal(operand1: operand1, operand2: operand2, previousOperation: operation)
                    operand1 = result
                }
                else {
                    operand1 = Double(lblResult.text!)!
                    result = operand1
                }
                lblOperation.text = checkNumber(number: formatPrecision(formated: result)) + " x "
            }
            if _sender.tag == 4 {
                if i > 1 {
                    operand2 = Double(lblResult.text!)!
                    if operand2 == 0 {
                        lblResult.text = "Error"
                    }
                    else{
                        result = calcalationSubTotal(operand1: operand1, operand2: operand2, previousOperation: operation)
                        operand1 = result
                    }
                    
                }
                else {
                    operand1 = Double(lblResult.text!)!
                    result = operand1
                }
                lblOperation.text = checkNumber(number: formatPrecision(formated: result)) + " ÷ "
            }
            if _sender.tag == 5 {
                if i > 1 {
                    operand2 = Double(lblResult.text!)!
                    result = calcalationSubTotal(operand1: operand1, operand2: operand2, previousOperation: operation)
                    operand1 = result
                }
                else {
                    operand1 = Double(lblResult.text!)!
                    result = operand1
                }
                lblOperation.text = checkNumber(number: formatPrecision(formated: result)) + " % "
            }
            operation = _sender.tag
            lblResult.text = "0"
        }
        else if _sender.tag == 0 {
            if operand2 == 0 || i > 0{
                operand2 = Double(lblResult.text!)!
            }
            if operation == 1 {
                lblOperation.text! += checkNumber(number: String(operand2))
                //lblResult.text = checkNumber(number: String(subTotal! + operand2))
                lblResult.text = checkNumber(number: String(result + operand2))
            }
            else if operation == 2 {
                lblOperation.text! += checkNumber(number: String(operand2))
                lblResult.text = checkNumber(number: String(result - operand2))
            }
            else if operation == 3 {
                lblOperation.text! += checkNumber(number: String(operand2))
                lblResult.text = checkNumber(number: String(result * operand2))
            }
            else if operation == 4 {
                lblOperation.text! += checkNumber(number: String(operand2))
                // format precision
                if operand2 == 0 {
                    lblResult.text = "Error"
                }
                else {
                    lblResult.text = checkNumber(number: formatPrecision(formated: (result / operand2)))
                }

            }
            else if operation == 5 {
                lblOperation.text! += checkNumber(number: String(operand2))
                lblResult.text = checkNumber(number: String(result.truncatingRemainder(dividingBy: operand2)))
            }
            operand2 = 0
            operand1 = 0
            operation = 0
            result = 0
            i = 0
        }
        else if _sender.tag == 7 {
            lblOperation.text = ""
            lblResult.text = "0"
            operand2 = 0
            operand1 = 0
            operation = 0
            result = 0
            i = 0
        }
    }
    // if 3.0 to 3
    func checkNumber(number : String) -> String {
        if number.contains("."){
            let result = number.split(separator: ".")
            if Int(result[1]) == 0 {
                return String(result[0])
            }
        }
        return number
    }
    func formatPrecision(formated : Double) -> String {
        var temp = String(formated).split(separator: ".")
        if temp[1].count > 5 {
            return String(format: "%.5f", formated)
        }
        return String(formated)
    }
    func calcalationSubTotal(operand1 : Double , operand2 : Double , previousOperation : Int) -> Double {
        var tempResult : Double = 0
        switch previousOperation {
        case 1:
            tempResult = operand1 + operand2
        case 2:
            tempResult = operand1 - operand2
        case 3:
            tempResult = operand1 * operand2
        case 4:
            tempResult = operand1 / operand2
        case 5:
            tempResult = operand1.truncatingRemainder(dividingBy: operand2)
        default:
            tempResult = 0
        }
        return tempResult
    }
}


